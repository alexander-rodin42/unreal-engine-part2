// Fill out your copyright notice in the Description page of Project Settings.

#include "Components/StaticMeshComponent.h"

#include "SnakeElementBase.h"
#include "SnakeBase.h"

DEFINE_LOG_CATEGORY_STATIC(LogSnakeElementBase, All, All)

// Sets default values
ASnakeElementBase::ASnakeElementBase()
{
    // Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
    PrimaryActorTick.bCanEverTick = true;

    MeshComponent = CreateDefaultSubobject<UStaticMeshComponent>("MeshComponent");
    MeshComponent->SetCollisionEnabled(ECollisionEnabled::QueryOnly);
    MeshComponent->SetCollisionResponseToChannels(ECR_Overlap);
    SetRootComponent(MeshComponent);
}

// Called when the game starts or when spawned
void ASnakeElementBase::BeginPlay()
{
    Super::BeginPlay();
    MeshComponent->OnComponentBeginOverlap.AddDynamic(this, &ASnakeElementBase::HandleBeginOverlap);
}

// Called every frame
void ASnakeElementBase::Tick(float DeltaTime)
{
    Super::Tick(DeltaTime);
}

void ASnakeElementBase::SetElementMaterialType_Implementation(const ESnakeElementType Type) {}

void ASnakeElementBase::Interact(AActor* Interactor, bool bIsHead)
{
    auto Snake = Cast<ASnakeBase>(Interactor);
    if (IsValid(Snake) && bIsHead)
    {
        Snake->Destroy();
    }
}

void ASnakeElementBase::HandleBeginOverlap(UPrimitiveComponent* OverlappedComponent,
                                           AActor* OtherActor,
                                           UPrimitiveComponent* OtherComponent,
                                           int32 OtherBodyIndex,
                                           bool bFromSweep,
                                           const FHitResult& SweepResult)
{
    if (IsValid(SnakeOwner))
    {
        UE_LOG(LogSnakeElementBase, Display, TEXT("%s overlaped %s"), *GetName(), *OtherActor->GetName());
        SnakeOwner->SnakeElementOverlap(this, OtherActor);
    }
}

void ASnakeElementBase::ToggleCollision()
{
    if (MeshComponent->GetCollisionEnabled() == ECollisionEnabled::NoCollision)
    {
        MeshComponent->SetCollisionEnabled(ECollisionEnabled::QueryOnly);
    }
    else
    {
        MeshComponent->SetCollisionEnabled(ECollisionEnabled::NoCollision);
    }
}
