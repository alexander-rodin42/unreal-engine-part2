// Fill out your copyright notice in the Description page of Project Settings.

#include "Components/StaticMeshComponent.h"

#include "SnakeBase.h"
#include "SnakeElementBase.h"
#include "Interactable.h"


DEFINE_LOG_CATEGORY_STATIC(LogSnakeBase, All, All)

// Sets default values
ASnakeBase::ASnakeBase()
    : ElementSize(100.f)
    , TickInterval(1.f)
    , SnakeStartingLength(4)
    , bIsForward(true)
    , LastMovementDirection(EMovementDirection::DOWN)
    , CurrentMovementDirection(LastMovementDirection)
    , LastTailLocation(0, 0, 0)
{
    // Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
    PrimaryActorTick.bCanEverTick = true;
}

// Called when the game starts or when spawned
void ASnakeBase::BeginPlay()
{
    Super::BeginPlay();
    SetActorTickInterval(TickInterval);
    CreateSnake(SnakeStartingLength);
}

// Called every frame
void ASnakeBase::Tick(float DeltaTime)
{
    Super::Tick(DeltaTime);
    Move();
}

void ASnakeBase::CreateSnake(const uint32 ElementsNumber)
{
    for (uint32 i = 0; i < ElementsNumber; ++i)
    {
        auto NewLocation = FVector(SnakeElements.Num() * ElementSize, 0, 0);
        auto NewFTaransform = FTransform(NewLocation);
        auto NewElement = GetWorld()->SpawnActor<ASnakeElementBase>(SnakeElementClass, NewFTaransform);
        NewElement->SetElementMaterialType(ESnakeElementType::BODY);
        SnakeElements.AddTail(NewElement);
        NewElement->SnakeOwner = this;
    }
    SnakeElements.GetTail()->GetValue()->SetElementMaterialType(ESnakeElementType::TAIL);
    SnakeElements.GetHead()->GetValue()->SetElementMaterialType(ESnakeElementType::HEAD);
}

void ASnakeBase::AddSnakeElement()
{
    auto NewFTaransform = FTransform(LastTailLocation);
    auto NewElement = GetWorld()->SpawnActor<ASnakeElementBase>(SnakeElementClass, NewFTaransform);
    NewElement->SetElementMaterialType(ESnakeElementType::TAIL);

    if (bIsForward)
    {
        SnakeElements.GetTail()->GetValue()->SetElementMaterialType(ESnakeElementType::BODY);
        SnakeElements.AddTail(NewElement);
    }
    else
    {
        SnakeElements.GetHead()->GetValue()->SetElementMaterialType(ESnakeElementType::BODY);
        SnakeElements.AddHead(NewElement);
    }

    NewElement->SnakeOwner = this;
}

bool ASnakeBase::IsElementsValid()
{
    for (const auto& Element : SnakeElements)
    {
        if (!IsValid(Element))
        {
            return false;
        }
    }

    return true;
}

FVector ASnakeBase::GetMovementVector(const EMovementDirection Direction)
{
    FVector Result(ForceInitToZero);

    switch (CurrentMovementDirection)
    {
        case EMovementDirection::UP:
            Result.X += ElementSize;
            break;
        case EMovementDirection::DOWN:
            Result.X -= ElementSize;
            break;
        case EMovementDirection::RIGHT:
            Result.Y += ElementSize;
            break;
        case EMovementDirection::LEFT:
            Result.Y -= ElementSize;
            break;
        default:
            break;
    }

    return Result;
}

void ASnakeBase::Move()
{
    if (!IsElementsValid())
    {
        UE_LOG(LogSnakeBase, Error, TEXT("Snake elements are not valid: ASnakeBase::Move()"));
        return;
    }

    TDoubleLinkedList<ASnakeElementBase*>::TDoubleLinkedListNode* SnakeHead;
    ToggleCollision();

    if (bIsForward)
    {
        SnakeHead = SnakeElements.GetHead();
        auto CurrentNode = SnakeElements.GetTail();
        LastTailLocation = CurrentNode->GetValue()->GetActorLocation();

        while (CurrentNode != SnakeHead)
        {
            auto NextNode = CurrentNode->GetPrevNode();
            FVector TargetLocation = NextNode->GetValue()->GetActorLocation();
            CurrentNode->GetValue()->SetActorLocation(TargetLocation);
            CurrentNode = NextNode;
        }
    }
    else
    {
        SnakeHead = SnakeElements.GetTail();
        auto CurrentNode = SnakeElements.GetHead();
        LastTailLocation = CurrentNode->GetValue()->GetActorLocation();

        while (CurrentNode != SnakeHead)
        {
            auto NextNode = CurrentNode->GetNextNode();
            FVector TargetLocation = NextNode->GetValue()->GetActorLocation();
            CurrentNode->GetValue()->SetActorLocation(TargetLocation);
            CurrentNode = NextNode;
        }
    }

    LastMovementDirection = CurrentMovementDirection;
    SnakeHead->GetValue()->AddActorLocalOffset(GetMovementVector(CurrentMovementDirection));
    ToggleCollision();
}

void ASnakeBase::ExpandDirection()
{
    if (!IsElementsValid())
    {
        UE_LOG(LogSnakeBase, Error, TEXT("Snake elements are not valid: ASnakeBase::ExpandDirection()"));
        return;
    }

    auto HeadElement = SnakeElements.GetHead();
    auto TailElement = SnakeElements.GetTail();

    if (bIsForward)
    {
        HeadElement->GetValue()->SetElementMaterialType(ESnakeElementType::TAIL);
        TailElement->GetValue()->SetElementMaterialType(ESnakeElementType::HEAD);

        CurrentMovementDirection = GetDirections(TailElement->GetValue(), TailElement->GetPrevNode()->GetValue());
        LastMovementDirection = InvertDirection(CurrentMovementDirection);
    }
    else
    {
        HeadElement->GetValue()->SetElementMaterialType(ESnakeElementType::HEAD);
        TailElement->GetValue()->SetElementMaterialType(ESnakeElementType::TAIL);

        CurrentMovementDirection = GetDirections(HeadElement->GetValue(), HeadElement->GetNextNode()->GetValue());
        LastMovementDirection = InvertDirection(CurrentMovementDirection);
    }
    bIsForward = !bIsForward;
}

EMovementDirection ASnakeBase::InvertDirection(const EMovementDirection Direction)
{
    if (Direction == EMovementDirection::UP)
    {
        return EMovementDirection::DOWN;
    }

    if (Direction == EMovementDirection::DOWN)
    {
        return EMovementDirection::UP;
    }

    if (Direction == EMovementDirection::RIGHT)
    {
        return EMovementDirection::LEFT;
    }

    if (Direction == EMovementDirection::LEFT)
    {
        return EMovementDirection::RIGHT;
    }

    UE_LOG(LogSnakeBase, Error, TEXT("Object invert error: ASnakeBase::InvertDirection()"));
    return EMovementDirection::NONE;
}

EMovementDirection ASnakeBase::GetDirections(const ASnakeElementBase* const HeadElement,
                                             const ASnakeElementBase* const PreviousElement)
{
    if (!IsValid(HeadElement) || !IsValid(PreviousElement))
    {
        UE_LOG(LogSnakeBase, Error, TEXT("Not a valid pointer: ASnakeBase::GetDirections()."));
        return EMovementDirection::NONE;
    }

    float HeadX = HeadElement->GetActorLocation().X;
    float HeadY = HeadElement->GetActorLocation().Y;
    float PreviousX = PreviousElement->GetActorLocation().X;
    float PreviousY = PreviousElement->GetActorLocation().Y;

    if (HeadX == PreviousX)
    {
        if (HeadY > PreviousY)
        {
            return EMovementDirection::RIGHT;
        }

        if (HeadY < PreviousY)
        {
            return EMovementDirection::LEFT;
        }
    }

    if (HeadY == PreviousY)
    {
        if (HeadX > PreviousX)
        {
            return EMovementDirection::UP;
        }

        if (HeadX < PreviousX)
        {
            return EMovementDirection::DOWN;
        }
    }

    UE_LOG(LogSnakeBase, Error,
           TEXT("Error determining movement direction: ASnakeBase::GetDirections(), element coordinates match."));
    return EMovementDirection::NONE;
}

void ASnakeBase::SnakeElementOverlap(ASnakeElementBase* OverlappedElement, AActor* OtherActor)
{
    if (!IsValid(OverlappedElement) || !IsValid(OtherActor))
    {
        return;
    }

    IInteractable* InteractableInterface = Cast<IInteractable>(OtherActor);
    if (!InteractableInterface)
    {
        return;
    }

    InteractableInterface->Interact(this, OverlappedElement == GetHead());
}

ASnakeElementBase* ASnakeBase::GetHead() const
{
    return bIsForward ? SnakeElements.GetHead()->GetValue() : SnakeElements.GetTail()->GetValue();
}

void ASnakeBase::ToggleCollision()
{
    if (bIsForward)
    {
        auto CurrentNode = SnakeElements.GetTail();
        while (CurrentNode != SnakeElements.GetHead())
        {
            CurrentNode->GetValue()->ToggleCollision();
            CurrentNode = CurrentNode->GetPrevNode();
        }
        SnakeElements.GetHead()->GetValue()->ToggleCollision();
    }
    else
    {
        for (auto i : SnakeElements)
        {
            i->ToggleCollision();
        }
    }
}
