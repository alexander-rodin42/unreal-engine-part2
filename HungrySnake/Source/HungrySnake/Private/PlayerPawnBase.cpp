// Fill out your copyright notice in the Description page of Project Settings.


#include "Components/SceneComponent.h"
#include "Camera/CameraComponent.h"
#include "Components/InputComponent.h"

#include "PlayerPawnBase.h"
#include "SnakeBase.h"
#include "SnakeElementBase.h"

// Sets default values
APlayerPawnBase::APlayerPawnBase()
{
    // Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
    PrimaryActorTick.bCanEverTick = true;

    SceneComponent = CreateDefaultSubobject<USceneComponent>(TEXT("SceneComponent"));
    SetRootComponent(SceneComponent);

    CameraComponent = CreateDefaultSubobject<UCameraComponent>(TEXT("CameraComponent"));
    CameraComponent->SetupAttachment(GetRootComponent());
}

// Called when the game starts or when spawned
void APlayerPawnBase::BeginPlay()
{
    Super::BeginPlay();

    CreateSnakeActor();
}

// Called every frame
void APlayerPawnBase::Tick(float DeltaTime)
{
    Super::Tick(DeltaTime);
}

// Called to bind functionality to input
void APlayerPawnBase::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
    Super::SetupPlayerInputComponent(PlayerInputComponent);

    if (IsValid(PlayerInputComponent))
    {
        PlayerInputComponent->BindAction("ReverseMovement", IE_Pressed, this, &APlayerPawnBase::ReverseMovement);
        PlayerInputComponent->BindAxis("VerticalMovement", this, &APlayerPawnBase::VerticalMovement);
        PlayerInputComponent->BindAxis("HorizontalMovement", this, &APlayerPawnBase::HorizontalMovement);
    }
}

void APlayerPawnBase::CreateSnakeActor()
{
    SnakeActor = GetWorld()->SpawnActor<ASnakeBase>(SnakeActorClass, FTransform());
}

void APlayerPawnBase::ReverseMovement()
{
    SnakeActor->ExpandDirection();
}

void APlayerPawnBase::VerticalMovement(const float Amount)
{
    if (IsValid(SnakeActor))
    {
        if (Amount > 0 && SnakeActor->LastMovementDirection != EMovementDirection::DOWN)
        {
            SnakeActor->CurrentMovementDirection = EMovementDirection::UP;
        }

        if (Amount < 0 && SnakeActor->LastMovementDirection != EMovementDirection::UP)
        {
            SnakeActor->CurrentMovementDirection = EMovementDirection::DOWN;
        }
    }
}

void APlayerPawnBase::HorizontalMovement(const float Amount)
{
    if (Amount > 0 && SnakeActor->LastMovementDirection != EMovementDirection::LEFT)
    {
        SnakeActor->CurrentMovementDirection = EMovementDirection::RIGHT;
    }

    if (Amount < 0 && SnakeActor->LastMovementDirection != EMovementDirection::RIGHT)
    {
        SnakeActor->CurrentMovementDirection = EMovementDirection::LEFT;
    }
}
