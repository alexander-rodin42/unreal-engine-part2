// Copyright Epic Games, Inc. All Rights Reserved.


#include "HungrySnakeGameModeBase.h"

#include "PlayerPawnBase.h"

AHungrySnakeGameModeBase::AHungrySnakeGameModeBase()
{
    DefaultPawnClass = APlayerPawnBase::StaticClass();
}