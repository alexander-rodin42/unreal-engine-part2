// Copyright Epic Games, Inc. All Rights Reserved.

#include "HungrySnake.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, HungrySnake, "HungrySnake" );
