// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Pawn.h"
#include "PlayerPawnBase.generated.h"

class USceneComponent;
class UCameraComponent;
class ASnakeBase;

UCLASS()
class HUNGRYSNAKE_API APlayerPawnBase : public APawn
{
    GENERATED_BODY()

public:
    // Sets default values for this pawn's properties
    APlayerPawnBase();

    UPROPERTY(BlueprintReadWrite)
    USceneComponent* SceneComponent;

    UPROPERTY(BlueprintReadWrite)
    UCameraComponent* CameraComponent;

    UPROPERTY(BlueprintReadWrite)
    ASnakeBase* SnakeActor;

    UPROPERTY(EditDefaultsOnly)
    TSubclassOf<ASnakeBase> SnakeActorClass;

protected:
    // Called when the game starts or when spawned
    virtual void BeginPlay() override;

public:
    // Called every frame
    virtual void Tick(float DeltaTime) override;

    // Called to bind functionality to input
    virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

    void CreateSnakeActor();
    void ReverseMovement();
    void VerticalMovement(const float Amount);
    void HorizontalMovement(const float Amount);
};
