// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Containers/List.h"
#include "SnakeBase.generated.h"

class ASnakeElementBase;

UENUM()
enum class EMovementDirection : uint8
{
    NONE,
    UP,
    DOWN,
    RIGHT,
    LEFT
};

UCLASS()
class HUNGRYSNAKE_API ASnakeBase : public AActor
{
    GENERATED_BODY()

public:
    // Sets default values for this actor's properties
    ASnakeBase();

    UPROPERTY(EditDefaultsOnly)
    TSubclassOf<ASnakeElementBase> SnakeElementClass;

    UPROPERTY(EditDefaultsOnly)
    float ElementSize;

    UPROPERTY(EditDefaultsOnly)
    float TickInterval;

    UPROPERTY(EditDefaultsOnly)
    uint32 SnakeStartingLength;

    UPROPERTY()
    bool bIsForward;

    TDoubleLinkedList<ASnakeElementBase*> SnakeElements;

    UPROPERTY()
    EMovementDirection LastMovementDirection;

    UPROPERTY()
    EMovementDirection CurrentMovementDirection;

    UPROPERTY()
    FVector LastTailLocation;

protected:
    // Called when the game starts or when spawned
    virtual void BeginPlay() override;

public:
    // Called every frame
    virtual void Tick(float DeltaTime) override;

    // The number of elements must be greater than 1
    void CreateSnake(const uint32 ElementsNumber = 2);
    void AddSnakeElement();

    bool IsElementsValid();
    FVector GetMovementVector(const EMovementDirection Direction);
    void Move();
    void ExpandDirection();
    EMovementDirection InvertDirection(const EMovementDirection Direction);
    EMovementDirection GetDirections(const ASnakeElementBase* const HeadElement,
                                     const ASnakeElementBase* const PreviousElement);
    void SnakeElementOverlap(ASnakeElementBase* OverlappedElement, AActor* OtherActor);
    ASnakeElementBase* GetHead() const;

    void ToggleCollision();
};
